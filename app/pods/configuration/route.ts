import Route from '@ember/routing/route';
import ConfigurationController from './controller';

export default class Configuration extends Route {
  setupController(controller: ConfigurationController) {
    controller.loadConfiguration();
  }
}
