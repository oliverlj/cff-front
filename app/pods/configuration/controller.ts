import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import BackService from 'front/services/back-service';
import { Changeset } from 'ember-changeset';
import FrontConfiguration from 'front/objects/front-configuration';

export default class ConfigurationController extends Controller {
  private static readonly EVENT_KEY = 'binary.location';

  @service backService!: BackService;

  @tracked
  configFileLocation: string | undefined;

  @tracked
  configuration = new FrontConfiguration();

  changeset = Changeset(this.configuration);

  @action
  async chooseBinaryLocation() {
    const binaryLocation = await this.backService.choose();
    this.changeset.set('binaryLocation', binaryLocation);
  }

  @action
  cancel() {
    this.changeset.rollback();
  }

  @action
  save() {
    this.changeset.save();
    console.log(this.configuration);

    if (this.configuration.binaryLocation) {
      localStorage.setItem(
        ConfigurationController.EVENT_KEY,
        this.configuration.binaryLocation
      );
    } else {
      localStorage.removeItem(ConfigurationController.EVENT_KEY);
    }

    this.configFileLocation = this.backService.saveConfiguration(
      this.configuration
    );
  }

  loadConfiguration() {
    const binaryLocation = localStorage.getItem(
      ConfigurationController.EVENT_KEY
    )!;

    if (binaryLocation) {
      this.configuration.binaryLocation = binaryLocation;
      const backConfiguration =
        this.backService.readConfiguration(binaryLocation);
      if (backConfiguration) {
        this.configuration.back = backConfiguration;
      }
    }
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'configuration-controller': ConfigurationController;
  }
}
