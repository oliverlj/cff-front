import Service from '@ember/service';
import FrontConfiguration from 'front/objects/front-configuration';
// const util = requireNode('util');
// const exec = util.promisify(requireNode('child_process').exec);
// const { dialog } = requireNode('electron');
// const { remote } = requireNode('electron');
// const { dialog } = remote.app; // or `const app = remote.app`

export default class BackService extends Service {
  async choose(): Promise<string> {
    const binary = await window.api.chooseBackBinary();
    return binary?.filePaths[0];
  }

  readConfiguration(binaryLocation: string) {
    if (binaryLocation) {
      return window.api.readConfiguration(binaryLocation);
    }

    return undefined;
  }

  saveConfiguration(config: FrontConfiguration) {
    const result = window.api.saveConfiguration(
      config.binaryLocation,
      config.back
    );

    if (result) {
      return result;
    }
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your services.
declare module '@ember/service' {
  interface Registry {
    back: Back;
  }
}
