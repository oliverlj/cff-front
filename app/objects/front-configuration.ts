import BackConfiguration from './back-configuration';

export default class FrontConfiguration {
  back = new BackConfiguration();
  binaryLocation: string | undefined;
}
