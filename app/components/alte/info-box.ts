import Component from '@glimmer/component';

interface AdminLteComponentsInfoBoxArgs {
  color?: string;
  icon?: string;
  iconPrefix?: string;
  loading: boolean;
}

export default class AdminLteComponentsInfoBox extends Component<AdminLteComponentsInfoBoxArgs> {}
