const { contextBridge, remote } = require('electron');
const { dialog } = remote;

const ApiConfiguration = require('./configuration.js');

contextBridge.exposeInMainWorld('api', {
  chooseBackBinary: () => {
    return dialog.showOpenDialog({
      properties: ['openFile'],
    });
  },

  readConfiguration: ApiConfiguration.readConfiguration,

  saveConfiguration: ApiConfiguration.saveConfiguration,
});
